import socket
 
def Main():
    host = "127.0.0.1" # Host ip
    port = 1111 # Port
     
    mySocket = socket.socket()
    mySocket.bind((host,port)) # Creat connection
     
    mySocket.listen(1) 
    conn, addr = mySocket.accept() # Accept connection from client
    print ("Connection from: " + str(addr))
    
    # Encode utf-8 then send to client
    conn.send(str("สวัสดีตอนเช้า จาก Python!").encode("utf-8"))
    
    while True:
            data = conn.recv(1024).decode()
            if not data:
                    break
            print ("From user (" + str(addr) + ") : " + str(data))
            data = input("Send to client -> ")
            print ("sending: " + str(data))
            conn.send(data.encode())
    conn.close() # close connection
     
if __name__ == '__main__':
    Main()
